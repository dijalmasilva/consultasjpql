/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.main;

import dijalmasilva.questao4.entidades.Carga;
import dijalmasilva.questao4.entidades.Transporte;
import dijalmasilva.questao4.jpa.JpaTransporte;
import java.util.List;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Consultas {

    public static void main(String[] args) {
        JpaTransporte dao = new JpaTransporte();
        List<Carga> cargasDeCajazeiras = dao.cargasDeCajazeiras();
        for (Carga carga : cargasDeCajazeiras) {
            System.out.println(carga.getDestino());
        }
    }
}
