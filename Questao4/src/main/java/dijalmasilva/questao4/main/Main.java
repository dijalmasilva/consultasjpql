/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.main;

import dijalmasilva.questao4.entidades.Carga;
import dijalmasilva.questao4.entidades.Comandante;
import dijalmasilva.questao4.entidades.Navio;
import dijalmasilva.questao4.entidades.Produto;
import dijalmasilva.questao4.entidades.Transporte;
import dijalmasilva.questao4.jpa.Jpa;
import java.time.LocalDate;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Main {

    public static void main(String[] args) {
        Jpa<Transporte> daoTransporte = new Jpa<>();
        
        Comandante c = new Comandante("Dijalma", "Brasil");
        Navio n1 = new Navio("Titanic", 3000, "Japão", c);
        Navio n2 = new Navio("Front", 222, "Brasil", c);
        
        Carga carga1 = new Carga("Cajazeiras", "Belem");
        Carga carga2 = new Carga("Cajazeiras", "São Paulo");
        Carga carga3 = new Carga("Natal", "Tocantins");
        
        Produto p1 = new Produto("Feijão", "Alimento");
        Produto p2 = new Produto("Arroz", "Alimento");
        Produto p3 = new Produto("Mandioca", "Alimento");
        
        carga1.addProduto(p1);
        carga1.addProduto(p2);
        carga1.addProduto(p3);
        
        Produto p4 = new Produto("Ferro", "Minerio");
        Produto p5 = new Produto("Zinco", "Minerio");
        Produto p6 = new Produto("Cobre", "Minerio");
        
        carga2.addProduto(p4);
        carga2.addProduto(p5);
        carga2.addProduto(p6);
        
        Produto p7 = new Produto("Macaco", "Animal");
        Produto p8 = new Produto("Girafa", "Animal");
        Produto p9 = new Produto("Zebra", "Animal");
        Produto p10 = new Produto("Leão", "Animal");
        Produto p11 = new Produto("Hipopótamo", "Animal");
        
        carga3.addProduto(p7);
        carga3.addProduto(p8);
        carga3.addProduto(p9);
        carga3.addProduto(p10);
        carga3.addProduto(p11);
        
        Transporte t1 = new Transporte(LocalDate.now(), 200, n1, carga3);
        Transporte t2 = new Transporte(LocalDate.now(), 200, n2, carga1);
        Transporte t3 = new Transporte(LocalDate.now(), 200, n1, carga2);
        
        daoTransporte.salvar(t1);
        daoTransporte.salvar(t2);
        daoTransporte.salvar(t3);
    }
}
