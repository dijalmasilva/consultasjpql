/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Transporte implements Serializable{


    @Id
    @GeneratedValue
    private int id;
    private LocalDate realizado;
    private double total;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Navio navio;
    @OneToOne(cascade = CascadeType.PERSIST)
    private Carga carga;

    public Transporte(LocalDate realizado, double total, Navio navio, Carga carga) {
        this.realizado = realizado;
        this.total = total;
        this.navio = navio;
        this.carga = carga;
    }

    public Transporte() {
    }

    public Transporte(int id, LocalDate realizado, double total, Navio navio, Carga carga) {
        this.id = id;
        this.realizado = realizado;
        this.total = total;
        this.navio = navio;
        this.carga = carga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getRealizado() {
        return realizado;
    }

    public void setRealizado(LocalDate realizado) {
        this.realizado = realizado;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Navio getNavio() {
        return navio;
    }

    public void setNavio(Navio navio) {
        this.navio = navio;
    }

    public Carga getCarga() {
        return carga;
    }

    public void setCarga(Carga carga) {
        this.carga = carga;
    }
    
    
}
