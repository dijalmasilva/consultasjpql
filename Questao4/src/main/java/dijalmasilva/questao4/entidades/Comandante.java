/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Comandante implements Serializable{


    @Id
    @GeneratedValue
    private int codNaval;
    private String nome;
    private String paisOrigem;
    @OneToMany(mappedBy = "comandante")
    private List<Navio> navios;

    public Comandante() {
    }

    public Comandante(String nome, String paisOrigem) {
        this.nome = nome;
        this.paisOrigem = paisOrigem;
    }

    public Comandante(String nome, String paisOrigem, List<Navio> navios) {
        this.nome = nome;
        this.paisOrigem = paisOrigem;
        this.navios = navios;
    }

    public int getCodNaval() {
        return codNaval;
    }

    public void setCodNaval(int codNaval) {
        this.codNaval = codNaval;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPaisOrigem() {
        return paisOrigem;
    }

    public void setPaisOrigem(String paisOrigem) {
        this.paisOrigem = paisOrigem;
    }

    public List<Navio> getNavios() {
        return navios;
    }

    public void setNavios(List<Navio> navios) {
        this.navios = navios;
    }
    
}
