/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Carga implements Serializable{

    @Id
    @GeneratedValue
    private int codigo;
    private String origem;
    private String destino;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Produto> produtos;

    public Carga() {
    }

    public Carga(String origem, String destino, List<Produto> produtos) {
        this.origem = origem;
        this.destino = destino;
        this.produtos = produtos;
    }

    public Carga(int codigo, String origem, String destino, List<Produto> produtos) {
        this.codigo = codigo;
        this.origem = origem;
        this.destino = destino;
        this.produtos = produtos;
    }

    public Carga(String origem, String destino) {
        this.origem = origem;
        this.destino = destino;
        this.produtos = new ArrayList<>();
    }

    public Carga(int codigo, String origem, String destino) {
        this.codigo = codigo;
        this.origem = origem;
        this.destino = destino;
        this.produtos = new ArrayList<>();
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public void addProduto(Produto p){
        this.produtos.add(p);
    }
}
