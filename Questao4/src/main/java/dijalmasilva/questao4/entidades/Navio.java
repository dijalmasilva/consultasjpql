/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Navio implements Serializable{

    @Id
    @GeneratedValue
    private int id;
    private String nome;
    private int capacidade;
    private String paisDeFabricacao;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private Comandante comandante;
    
    @OneToMany(mappedBy = "navio")
    private List<Transporte> transportes;

    public Navio() {
    }

    public Navio(String nome, int capacidade, String paisDeFabricacao, Comandante comandante) {
        this.nome = nome;
        this.capacidade = capacidade;
        this.paisDeFabricacao = paisDeFabricacao;
        this.comandante = comandante;
        this.transportes = new ArrayList<>();
    }

    public Navio(String nome, int capacidade, String paisDeFabricacao, Comandante comandante, List<Transporte> transportes) {
        this.nome = nome;
        this.capacidade = capacidade;
        this.paisDeFabricacao = paisDeFabricacao;
        this.comandante = comandante;
        this.transportes = transportes;
    }

    public Navio(int id, String nome, int capacidade, String paisDeFabricacao, Comandante comandante) {
        this.id = id;
        this.nome = nome;
        this.capacidade = capacidade;
        this.paisDeFabricacao = paisDeFabricacao;
        this.comandante = comandante;
        this.transportes = new ArrayList<>();
    }

    public Navio(int id, String nome, int capacidade, String paisDeFabricacao, Comandante comandante, List<Transporte> transportes) {
        this.id = id;
        this.nome = nome;
        this.capacidade = capacidade;
        this.paisDeFabricacao = paisDeFabricacao;
        this.comandante = comandante;
        this.transportes = transportes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public String getPaisDeFabricacao() {
        return paisDeFabricacao;
    }

    public void setPaisDeFabricacao(String paisDeFabricacao) {
        this.paisDeFabricacao = paisDeFabricacao;
    }

    public Comandante getComandante() {
        return comandante;
    }

    public void setComandante(Comandante comandante) {
        this.comandante = comandante;
    }

    public List<Transporte> getTransportes() {
        return transportes;
    }

    public void setTransportes(List<Transporte> transportes) {
        this.transportes = transportes;
    }
}
