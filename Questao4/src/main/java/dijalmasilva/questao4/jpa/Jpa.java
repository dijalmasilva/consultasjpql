/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Jpa<T> {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("Questao4");
    private final EntityManager em = factory.createEntityManager();
    
    public void salvar(T obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
    }
}
