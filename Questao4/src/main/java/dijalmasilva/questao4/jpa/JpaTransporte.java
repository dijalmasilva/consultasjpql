/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao4.jpa;

import dijalmasilva.questao4.entidades.Carga;
import dijalmasilva.questao4.entidades.Transporte;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaTransporte {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("Questao4");
    private final EntityManager em = factory.createEntityManager();
    
    public List<Carga> cargasDeCajazeiras(){
        TypedQuery<Carga> query = em.createQuery("SELECT c FROM Transporte t, t.carga c WHERE c.origem = :cidade and t.realizado between :data1 and :data2", Carga.class);
        query.setParameter("cidade", "Cajazeiras");
        query.setParameter("data1", LocalDate.of(2014, Month.JANUARY, 1));
        query.setParameter("data2", LocalDate.of(2014, Month.OCTOBER, 12));
        return query.getResultList();
    }
}
