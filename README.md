#**Atividade Avaliativa Consultas**#

## **Alunos** ##

Manoel Dijalma / Wellignton Lins

### Respostas ###

* Primeira Questão

Letra a:
```
#!SQL

SELECT L FROM Livro L JOIN L.autores A WHERE a.dataNascimento <> :data
```
Letra b: 
```
#!SQL

SELECT DISTINCT P FROM Professor P , IN (P.telefones) T WHERE T.numero IS NOT NULL and P.endereco.rua like :rua
```
Letra c: 
```
#!SQL

SELECT new dijalmasilva.atividadeavaliativajpql.entidades.AlunoVO(a.cpf, a.nome, a.idade) from Aluno a where a.turma like '2011.1'
```
Letra d: 
```
#!Sql

SELECT DISTINCT P FROM Professor P ,IN (P.telefones) t  where t.numero like :telefone"
```

* Segunda Questão

     		Apenas mapeada

* Terceira Questão

			Terceira questão upada

* Quarta Questão

Letra a:
```
#!SQL

SELECT c FROM Transporte t, t.carga c WHERE c.origem = :cidade and t.realizado between :data1 and :data2
```

* Quinta Questão

Letra a:
```
#!SQL

SELECT m.titulo FROM MaterialDidatico m WHERE m.dataCadastro = :hoje	
```

Letra c:
```
#!SQL
SELECT m FROM MaterialDidatico m WHERE M.publico = true AND M.tipo = :tipo
```