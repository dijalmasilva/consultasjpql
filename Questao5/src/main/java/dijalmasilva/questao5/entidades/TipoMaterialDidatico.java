/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijalmasilva.questao5.entidades;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public enum TipoMaterialDidatico {

    APOSTILA, VIDEOAULA, APRESENTACAO;
}
