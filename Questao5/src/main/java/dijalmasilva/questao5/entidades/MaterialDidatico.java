/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao5.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class MaterialDidatico implements Serializable{

    @Id
    @Column(length = 45)
    private String origem;
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BIGINT")
    private long codigo;
    @Column(length = 155)
    private String titulo;
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDate dataCadastro;
    @Column(columnDefinition = "TEXT")
    private String descricao;
    @Column(length = 55)
    private String autor;
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private TipoMaterialDidatico tipo;
    private boolean publico;
    @Column(columnDefinition = "DECIMAL(6,2)")
    private double tamanho;

    public MaterialDidatico() {
    }

    public MaterialDidatico(String origem, long codigo, String titulo, LocalDate dataCadastro, String descricao, String autor, TipoMaterialDidatico tipo, boolean publico, double tamanho) {
        this.origem = origem;
        this.codigo = codigo;
        this.titulo = titulo;
        this.dataCadastro = dataCadastro;
        this.descricao = descricao;
        this.autor = autor;
        this.tipo = tipo;
        this.publico = publico;
        this.tamanho = tamanho;
    }

    public MaterialDidatico(String origem, String titulo, LocalDate dataCadastro, String descricao, String autor, TipoMaterialDidatico tipo, boolean publico, double tamanho) {
        this.origem = origem;
        this.titulo = titulo;
        this.dataCadastro = dataCadastro;
        this.descricao = descricao;
        this.autor = autor;
        this.tipo = tipo;
        this.publico = publico;
        this.tamanho = tamanho;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public TipoMaterialDidatico getTipo() {
        return tipo;
    }

    public void setTipo(TipoMaterialDidatico tipo) {
        this.tipo = tipo;
    }

    public boolean isPublico() {
        return publico;
    }

    public void setPublico(boolean publico) {
        this.publico = publico;
    }

    public double getTamanho() {
        return tamanho;
    }

    public void setTamanho(double tamanho) {
        this.tamanho = tamanho;
    }
    
    
}
