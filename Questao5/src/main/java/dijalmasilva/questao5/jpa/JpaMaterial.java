/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao5.jpa;

import dijalmasilva.questao5.entidades.MaterialDidatico;
import dijalmasilva.questao5.entidades.TipoMaterialDidatico;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaMaterial {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("Questao5");
    private final EntityManager em = factory.createEntityManager();
    
    public void salvar(MaterialDidatico obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        
    }
    
    public Map<String, Object> materiaisDeHoje(){
        TypedQuery<MaterialDidatico> query = em.createQuery("SELECT m.titulo FROM MaterialDidatico m WHERE m.dataCadastro = :hoje", MaterialDidatico.class);
        query.setParameter("hoje", LocalDate.now());
        return query.getHints();
    }
    
    public List<MaterialDidatico> todosPublicosAndApostilas(){
        TypedQuery<MaterialDidatico> query = em.createQuery("SELECT m FROM MaterialDidatico m WHERE M.publico = true AND M.tipo = :tipo", MaterialDidatico.class);
        query.setParameter("tipo", TipoMaterialDidatico.APOSTILA);
        return query.getResultList();
    }
}
