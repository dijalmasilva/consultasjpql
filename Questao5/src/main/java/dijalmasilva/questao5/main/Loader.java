/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijalmasilva.questao5.main;

import dijalmasilva.questao5.entidades.MaterialDidatico;
import dijalmasilva.questao5.entidades.TipoMaterialDidatico;
import dijalmasilva.questao5.jpa.JpaMaterial;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Loader {

    public static void main(String[] args) {
        JpaMaterial dao = new JpaMaterial();
        //        
        //        MaterialDidatico m = new MaterialDidatico("Meu PC", "Livro JPA", LocalDate.now(), "Completo", "DIJALMA", TipoMaterialDidatico.APOSTILA, true, 32.2);
        //        MaterialDidatico m2 = new MaterialDidatico("WebSite", "Consultas JPA", LocalDate.now(), "Como fazer consultas.", "DIJALMA", TipoMaterialDidatico.VIDEOAULA, true, 2133.2);
        //        
        //        dao.salvar(m);
        //        dao.salvar(m2);
        //        Map<String, Object> materiaisDeHoje = dao.materiaisDeHoje();
        //        materiaisDeHoje.entrySet().stream().forEach((entry) -> {
        //            System.out.println("Chave: " + entry.getKey() + " - Valor: " + entry.getValue());
        //        });
        List<MaterialDidatico> apostilasPublicas = dao.todosPublicosAndApostilas();
        for (MaterialDidatico a : apostilasPublicas) {
            System.out.println(a.getTitulo());
        }
    }
}
