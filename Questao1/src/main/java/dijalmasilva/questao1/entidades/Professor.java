/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.entidades;

import dijalmasilva.questao1.enums.Telefone;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Professor extends Pessoa{

    private double salario;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Telefone> telefones;

    public Professor() {
    }

    public Professor(double salario, List<Telefone> telefones, String cpf, String nome, int idade, LocalDate dataNascimento, Endereco endereco) {
        super(cpf, nome, idade, dataNascimento, endereco);
        this.salario = salario;
        this.telefones = telefones;
    }

    public Professor(double salario, List<Telefone> telefones, int id, String cpf, String nome, int idade, LocalDate dataNascimento, Endereco endereco) {
        super(id, cpf, nome, idade, dataNascimento, endereco);
        this.salario = salario;
        this.telefones = telefones;
    }

    public Professor(double salario, String cpf, String nome, int idade, LocalDate dataNascimento, Endereco endereco) {
        super(cpf, nome, idade, dataNascimento, endereco);
        this.salario = salario;
        this.telefones = new ArrayList<>();
    }

    public Professor(double salario, int id, String cpf, String nome, int idade, LocalDate dataNascimento, Endereco endereco) {
        super(id, cpf, nome, idade, dataNascimento, endereco);
        this.salario = salario;
        this.telefones = new ArrayList<>();
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public void addTelefone(Telefone telefone){
        this.telefones.add(telefone);
    }

    @Override
    public String toString() {
        return "Professor{" + "salario=" + salario + ", telefones=" + telefones + '}';
    }
}
