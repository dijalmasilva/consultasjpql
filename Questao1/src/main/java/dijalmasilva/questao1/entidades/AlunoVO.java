/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.entidades;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class AlunoVO {

    private String cpf;
    private String nome;
    private int idade;

    public AlunoVO(String cpf, String nome, int idade) {
        this.cpf = cpf;
        this.nome = nome;
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return "AlunoVO{" + "cpf=" + cpf + ", nome=" + nome + ", idade=" + idade + '}';
    }
    
}
