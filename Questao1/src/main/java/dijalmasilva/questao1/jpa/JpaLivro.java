/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.jpa;

import dijalmasilva.questao1.entidades.Livro;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaLivro {

    private final EntityManagerFactory factory;
    private final EntityManager em;

    public JpaLivro() {
        this.factory = Persistence.createEntityManagerFactory("AtividadeJPQL");
        this.em = factory.createEntityManager();
    }
    
    public boolean salvar(Livro obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        return true;
    }
    
    public boolean remover(Livro obj){
        em.getTransaction().begin();
        em.remove(em.merge(obj));
        em.getTransaction().commit();
        return true;
    }
    
    public Livro buscar(int key){
        return em.find(Livro.class, key);
    }
    
    public List<Livro> noActorIn82(){
        LocalDate data = LocalDate.of(1982, Month.NOVEMBER, 21);
        TypedQuery<Livro> query = em.createQuery("SELECT L FROM Livro L JOIN L.autores A WHERE a.dataNascimento <> :data", Livro.class);
        query.setParameter("data", data);
        return query.getResultList();
    }
}
