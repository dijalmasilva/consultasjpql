/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.jpa;

import dijalmasilva.questao1.entidades.Aluno;
import dijalmasilva.questao1.entidades.AlunoVO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaAluno {

    private final EntityManagerFactory factory;
    private final EntityManager em;

    public JpaAluno() {
        this.factory = Persistence.createEntityManagerFactory("AtividadeJPQL");
        this.em = factory.createEntityManager();
    }
    
    public boolean salvar(Aluno obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        return true;
    }
    
    public boolean remover(Aluno obj){
        em.getTransaction().begin();
        em.remove(em.merge(obj));
        em.getTransaction().commit();
        return true;
    }
    
    public Aluno buscar(int key){
        return em.find(Aluno.class, key);
    }
    
    public List<AlunoVO> q1c() {
        Query query = em.createQuery("SELECT new "
                + "dijalmasilva.atividadeavaliativajpql.entidades.AlunoVO"
                + "(a.cpf, a.nome, a.idade) from Aluno a where a.turma like '2011.1'");
        List<AlunoVO> lista = query.getResultList();
        return  lista;
    }
}
