/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.jpa;

import dijalmasilva.questao1.entidades.Autor;
import dijalmasilva.questao1.entidades.Livro;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaAutor {

    private final EntityManagerFactory factory;
    private final EntityManager em;

    public JpaAutor() {
        this.factory = Persistence.createEntityManagerFactory("AtividadeJPQL");
        this.em = factory.createEntityManager();
    }
    
    public boolean salvar(Autor obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        return true;
    }
    
    public boolean remover(Autor obj){
        em.getTransaction().begin();
        em.remove(em.merge(obj));
        em.getTransaction().commit();
        return true;
    }
    
    public Autor buscar(int key){
        return em.find(Autor.class, key);
    }
    
    public List<Livro> q1e() {
        Query query = em.createQuery("SELECT L FROM Livro L ,IN (L.autores) a "
                + "where a.endereco.cidade like :cidade AND l.lancamento between :data1 and :data2");
        
        query.setParameter("cidade", "Cajazeiras");
        query.setParameter("data1", LocalDate.of(2011, Month.JANUARY, 1));
        query.setParameter("data2", LocalDate.of(2011, Month.DECEMBER, 12));
        List<Livro> lista = query.getResultList();
        return  lista;
    }
}
