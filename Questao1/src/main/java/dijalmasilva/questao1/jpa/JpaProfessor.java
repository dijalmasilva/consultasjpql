/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao1.jpa;

import dijalmasilva.questao1.entidades.Professor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class JpaProfessor {

    private final EntityManagerFactory factory;
    private final EntityManager em;

    public JpaProfessor() {
        this.factory = Persistence.createEntityManagerFactory("AtividadeJPQL");
        this.em = factory.createEntityManager();
    }
    
    public boolean salvar(Professor obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        return true;
    }
    
    public boolean remover(Professor obj){
        em.getTransaction().begin();
        em.remove(em.merge(obj));
        em.getTransaction().commit();
        return true;
    }
    
    public Professor buscar(int key){
        return em.find(Professor.class, key);
    }
    
    public List<Professor> professoresTelInRua(){
        TypedQuery query = em.createQuery("SELECT DISTINCT P FROM Professor P , IN (P.telefones) T"
                + " WHERE T.numero IS NOT NULL and P.endereco.rua like :rua ", Professor.class);
        query.setParameter("rua", "Que prova fácil");
        return query.getResultList();
    }
    
    public List<Professor> telFinal8() {
        TypedQuery query = em.createQuery("SELECT DISTINCT P FROM Professor P ,IN (P.telefones)"
                + " t  where t.numero like :telefone", Professor.class);
        query.setParameter("telefone", "%8");
        List<Professor> lista = query.getResultList();
        return  lista;
    }
}
