/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijalmasilva.questao1.main;

import dijalmasilva.questao1.entidades.Livro;
import dijalmasilva.questao1.jpa.JpaAutor;
import java.util.List;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Loader {

    public static void main(String[] args) {

//        Aluno aluno = new Aluno("203231010134", LocalDate.of(2013, Month.MAY, 13), "2011.1", "212.121.121-12", "Joe",
//                21, LocalDate.of(1995, Month.JANUARY, 26), new Endereco("Tenente Aquino de Albuquerque",
//                "Casas Populares", "Cajazeiras", "58900-000"));
//        Aluno aluno2 = new Aluno("203238010134", LocalDate.of(2013, Month.MAY, 13), "2011.1", "212.121.121-12", "Welligton",
//                21, LocalDate.of(1995, Month.JANUARY, 26), new Endereco("Tenente Aquino de Albuquerque",
//                "Casas Populares", "Cajazeiras", "58900-000"));
//        Professor professor = new Professor(5000, "233.333.112-22", "Job", 28, LocalDate.of(1988, Month.MARCH, 10), 
//                new Endereco("Que prova fácil", "Centro", "Cajazeiras", "58900-000"));
//        Professor professor2 = new Professor(5000, "233.444.112-22","Diogo", 28, LocalDate.of(1988, Month.MARCH, 28), 
//                new Endereco("Que prova fácil", "Oasis", "Cajazeiras", "58900-000"));
//        Telefone telefone = new Telefone("3531-2123", TelefoneType.RESIDENDECIAL);
//        Telefone comercial = new Telefone("3531-1222", TelefoneType.COMERCIAL);
//        Telefone tel1 = new Telefone("8383-8888", TelefoneType.COMERCIAL);
//        professor.addTelefone(telefone);
//        professor.addTelefone(comercial);
//        professor2.addTelefone(tel1);
//        Autor autor = new Autor("IFPB","444.333.112-44", "Dijalma", 34, LocalDate.of(1982, Month.NOVEMBER, 21), 
//                new Endereco("Qualquer", "Centro", "Ilford", "28182-222"));
//        Livro livro = new Livro("JPA", "9232500207713", LocalDate.of(2009, Month.NOVEMBER, 20));
//        
//        autor.addLivro(livro);
//        livro.addAutor(autor);
//
//        JpaAluno daoAluno = new JpaAluno();
//        daoAluno.salvar(aluno);
//        daoAluno.salvar(aluno2);
//        List<AlunoVO> alunosVO = daoAluno.q1c();
//        alunosVO.forEach(System.out::println);
//        JpaProfessor daoProfessor = new JpaProfessor();
//        List<Professor> prof8 = daoProfessor.telFinal8();
//        prof8.stream().forEach((professor) -> {
//            System.out.println(professor.getNome());
//        });
//        List<Professor> professoresTelInRua = daoProfessor.professoresTelInRua();
//        System.out.println(professoresTelInRua.size());
//        System.out.println(professoresTelInRua.get(0).getNome());
//        System.out.println(professoresTelInRua.get(1).getNome());
//        daoProfessor.salvar(professor);
//        daoProfessor.salvar(professor2);
//        JpaLivro daoLivro = new JpaLivro();
//        List<Livro> livros = daoLivro.noActorIn82();
//        System.out.println(livros.size());
//        daoLivro.salvar(livro);
//        System.out.println("Deu certo!!");
//        Endereco e = new Endereco("Alguma coisa", "Algum bairro", "Cajazeiras", "58900-000");
//        Autor walter = new Autor("Universidade de Harvad", "123.123.333-11", "Walter Isaacson", 63, LocalDate.of(1952, Month.MAY, 20), e);
//        Autor jose = new Autor("Uiraúna", "133.323.222-99", "José Nêumanne Pinto", 66, LocalDate.of(1951, Month.MAY, 18), e);
//        JpaAutor daoAutor = new JpaAutor();
//        Livro steve = new Livro("Steve Jobs", "9781451648539", LocalDate.of(2011, Month.OCTOBER, 24));
//        Livro lula = new Livro("O que sei de Lula", "9222451648539", LocalDate.of(2011, Month.JANUARY, 1));
//        steve.addAutor(walter);
//        lula.addAutor(jose);
//        walter.addLivro(steve);
//        jose.addLivro(lula);
//        daoAutor.salvar(walter);
//        daoAutor.salvar(jose);
//        JpaLivro daoLivro = new JpaLivro();
//        daoLivro.salvar(steve);
//        daoLivro.salvar(lula);
//        List<Livro> livros = daoAutor.q1e();
//        
//        livros.stream().forEach((livro) -> {
//            System.out.println(livro.getTitulo());
//        });
    }
}
