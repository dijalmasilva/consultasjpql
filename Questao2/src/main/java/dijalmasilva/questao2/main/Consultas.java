/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.main;

import dijalmasilva.questao2.entidades.Publicacao;
import dijalmasilva.questao2.jpa.Jpa;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Consultas {

    public static void main(String[] args) {
        
        //Questão 2 - primeira consulta
        
        Jpa<Publicacao> daoJpa = new Jpa<>();
        
        //Letra a
        Map<String, Object> parametros = new HashMap<>();

        daoJpa.consultaLista("SELECT E.NOME, P.TITULO, A.NOME FROM  PUBLICACAO P, IN (P.AREA) A, IN (P.ESCRITOR) E  WHERE A.COD = :cod", 
                Publicacao.class, parametros);
    }
}
