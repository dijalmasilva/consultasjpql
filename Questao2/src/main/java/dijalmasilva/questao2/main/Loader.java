/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.main;

import dijalmasilva.questao2.entidades.Area;
import dijalmasilva.questao2.entidades.Escritor;
import dijalmasilva.questao2.entidades.Publicacao;
import dijalmasilva.questao2.entidades.Revisor;
import dijalmasilva.questao2.jpa.Jpa;
import java.time.LocalDate;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class Loader {

    public static void main(String[] args) {
        
        //Persistindo areas
        Area informatica = new Area("Informática");
        Area psicologia = new Area("Psicologia");
        Area matematica = new Area("Matemática");
        Area psicomatica = new Area("Psicomatica");
        Area industria = new Area("Indústria");
        
        Jpa<Area> daoArea = new Jpa<>();
        daoArea.salvar(informatica);
        daoArea.salvar(psicologia);
        daoArea.salvar(matematica);
        daoArea.salvar(industria);
        
        Escritor job = new Escritor(10, "Job", LocalDate.now());
        Escritor yara = new Escritor(8, "Yara", LocalDate.now());
        Escritor sonildo = new Escritor(9, "Sonildo", LocalDate.now());
        Escritor dijalma = new Escritor(20, "Dijalma", LocalDate.now());
        Escritor saulo = new Escritor(14, "Saulo", LocalDate.now());
        
//        Jpa<Escritor> daoEscritor = new Jpa<>();
//        daoEscritor.salvar(job);
//        daoEscritor.salvar(yara);
//        daoEscritor.salvar(sonildo);
//        daoEscritor.salvar(dijalma);
//        daoEscritor.salvar(saulo);
        
        Publicacao jpa = new Publicacao("JPA 2.1", job);
        jpa.addArea(informatica);
        Publicacao menteLouca = new Publicacao("Cerébro Louco", yara);
        menteLouca.addArea(psicologia);
        Publicacao integrais = new Publicacao("Integrais", sonildo);
        integrais.addArea(matematica);
        Publicacao derivadas = new Publicacao("Derivando a política", dijalma);
        derivadas.addArea(psicologia);
        derivadas.addArea(matematica);
        Publicacao industrializacao = new Publicacao("Industrialização acaba com solos", saulo);
        industrializacao.addArea(industria);
        Publicacao java = new Publicacao("Java 8", job);
        java.addArea(informatica);
        
        Jpa<Publicacao> daoPublicacao = new Jpa<>();
        daoPublicacao.salvar(jpa);
        daoPublicacao.salvar(menteLouca);
        daoPublicacao.salvar(integrais);
        daoPublicacao.salvar(derivadas);
        daoPublicacao.salvar(industrializacao);
        daoPublicacao.salvar(java);
        
        job.addPublicacoes(jpa);
        job.addPublicacoes(java);
        yara.addPublicacoes(menteLouca);
        sonildo.addPublicacoes(integrais);
        dijalma.addPublicacoes(derivadas);
        saulo.addPublicacoes(industrializacao);
        
        Revisor r1 = new Revisor("9", "Revisor 1", LocalDate.now());
        Revisor r2 = new Revisor("9.6", "Revisor 1", LocalDate.now());
        Revisor r3 = new Revisor("8.9", "Revisor 2", LocalDate.now());
        Revisor r4 = new Revisor("10", "Revisor 3", LocalDate.now());
        Revisor r5 = new Revisor("9.8", "Revisor 3", LocalDate.now());
        Revisor r6 = new Revisor("9.9", "Revisor 1", LocalDate.now());
        
        r1.addPublicacao(jpa);
        r1.addPublicacao(derivadas);
        r2.addPublicacao(menteLouca);
        r3.addPublicacao(integrais);
        r4.addPublicacao(industrializacao);
        r5.addPublicacao(java);
        
        Jpa<Revisor> daoRevisor = new Jpa<>();
        daoRevisor.salvar(r1);
        daoRevisor.salvar(r2);
        daoRevisor.salvar(r3);
        daoRevisor.salvar(r4);
        daoRevisor.salvar(r5);
        daoRevisor.salvar(r6);
    }
}
