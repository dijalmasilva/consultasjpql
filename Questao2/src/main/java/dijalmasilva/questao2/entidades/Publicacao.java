/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Publicacao implements Serializable{

    @Id
    @GeneratedValue
    private int codPublicacao;
    private String titulo;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Escritor escritor;
    @OneToMany
    private List<Area> areas;

    public Publicacao() {
    }

    public Publicacao(String titulo, Escritor escritor) {
        this.titulo = titulo;
        this.escritor = escritor;
        this.areas = new ArrayList<>();
    }
    
    public Publicacao(int codPublicacao, String titulo, Escritor escritor) {
        this.codPublicacao = codPublicacao;
        this.titulo = titulo;
        this.escritor = escritor;
        this.areas = new ArrayList<>();
    }

    public Publicacao(int codPublicacao, String titulo, Escritor escritor, List<Area> areas) {
        this.codPublicacao = codPublicacao;
        this.titulo = titulo;
        this.escritor = escritor;
        this.areas = areas;
    }

    public Publicacao(String titulo, Escritor escritor, List<Area> areas) {
        this.titulo = titulo;
        this.escritor = escritor;
        this.areas = areas;
    }

    public int getCodPublicacao() {
        return codPublicacao;
    }

    public void setCodPublicacao(int codPublicacao) {
        this.codPublicacao = codPublicacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Escritor getEscritor() {
        return escritor;
    }

    public void setEscritor(Escritor escritor) {
        this.escritor = escritor;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public void addArea(Area a){
        this.areas.add(a);
    }
    
}
