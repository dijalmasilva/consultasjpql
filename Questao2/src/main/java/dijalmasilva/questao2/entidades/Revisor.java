/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.entidades;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Revisor extends Pessoa{

    private String nota;
    @OneToMany
    private List<Publicacao> publicacoes;

    public Revisor() {
    }

    public Revisor(String nota, String nome, LocalDate dataNascimento) {
        super(nome, dataNascimento);
        this.nota = nota;
        this.publicacoes = new ArrayList<>();
    }

    public Revisor(String nota, List<Publicacao> publicacoes, String nome, LocalDate dataNascimento) {
        super(nome, dataNascimento);
        this.nota = nota;
        this.publicacoes = publicacoes;
    }

    public Revisor(String nota, List<Publicacao> publicacoes, int id, String nome, LocalDate dataNascimento) {
        super(id, nome, dataNascimento);
        this.nota = nota;
        this.publicacoes = publicacoes;
    }

    public Revisor(String nota, int id, String nome, LocalDate dataNascimento) {
        super(id, nome, dataNascimento);
        this.nota = nota;
        this.publicacoes = new ArrayList<>();
    }
    
    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public List<Publicacao> getPublicacoes() {
        return publicacoes;
    }

    public void setPublicacoes(List<Publicacao> publicacoes) {
        this.publicacoes = publicacoes;
    }
    
    public void addPublicacao(Publicacao publicacao){
        this.publicacoes.add(publicacao);
    }
}
