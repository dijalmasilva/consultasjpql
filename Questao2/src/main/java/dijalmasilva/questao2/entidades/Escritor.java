/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.entidades;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Escritor extends Pessoa{
    
    private int premios;
    
    @OneToMany(mappedBy = "escritor")
    private List<Publicacao> publicacoes;
    
    public Escritor() {
    }

    public Escritor(int premios, List<Publicacao> publicacoes, String nome, LocalDate dataNascimento) {
        super(nome, dataNascimento);
        this.premios = premios;
        this.publicacoes = publicacoes;
    }

    public Escritor(int premios, List<Publicacao> publicacoes, int id, String nome, LocalDate dataNascimento) {
        super(id, nome, dataNascimento);
        this.premios = premios;
        this.publicacoes = publicacoes;
    }

    public Escritor(int premios, String nome, LocalDate dataNascimento) {
        super(nome, dataNascimento);
        this.premios = premios;
        this.publicacoes = new ArrayList<>();
    }

    public Escritor(int premios, int id, String nome, LocalDate dataNascimento) {
        super(id, nome, dataNascimento);
        this.premios = premios;
        this.publicacoes = new ArrayList<>();
    }

    public int getPremios() {
        return premios;
    }

    public void setPremios(int premios) {
        this.premios = premios;
    }

    public List<Publicacao> getPublicacoes() {
        return publicacoes;
    }

    public void setPublicacoes(List<Publicacao> publicacoes) {
        this.publicacoes = publicacoes;
    }
    
    public void addPublicacoes(Publicacao publicacao){
        this.publicacoes.add(publicacao);
    }
}
