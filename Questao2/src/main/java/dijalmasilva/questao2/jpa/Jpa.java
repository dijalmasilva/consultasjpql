/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao2.jpa;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 * @param <T>
 */
public class Jpa<T> {
    
    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("Questao2");
    private final EntityManager em = factory.createEntityManager();
    
    public boolean salvar(T obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        return true;
    }
    
    public List<T> consultaLista(String consulta, Class<T> classe, Map<String, Object> parametros) {
        TypedQuery<T> query = em.createQuery(consulta, classe);
        if (parametros != null && !parametros.isEmpty()) {
            for (String parametro : parametros.keySet()) {
                query.setParameter(parametro, parametros.get(parametro));
            }
        }
        return query.getResultList();
    }

    public T consultaSimples(String consulta, Class<T> classe, Map<String, Object> parametros) {
        try {
            TypedQuery<T> query = em.createQuery(consulta, classe);
            if (parametros != null && !parametros.isEmpty()) {
                for (String parametro : parametros.keySet()) {
                    query.setParameter(parametro, parametros.get(parametro));
                }
            }
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    
}
