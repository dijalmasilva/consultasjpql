/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao3.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
//Exemplo de SQLMapping
@SqlResultSetMapping(name = "MapAutor", 
        entities = @EntityResult(
                entityClass = Autor.class,
                fields = {
                    @FieldResult(name = "id", column = "idAutor"),
                    @FieldResult(name = "nome", column = "nome"),
                    @FieldResult(name = "sobrenome", column = "nobrenome"),
                    @FieldResult(name = "versao", column = "versao")}))
public class Autor implements Serializable{
    
    @Id
    @GeneratedValue
    private Long id;
    private int versao;
    private String nome;
    private String sobrenome;

    public Autor() {
    }

    public Autor(int versao, String nome, String sobrenome) {
        this.versao = versao;
        this.nome = nome;
        this.sobrenome = sobrenome;
    }

    public Autor(Long id, int versao, String nome, String sobrenome) {
        this.id = id;
        this.versao = versao;
        this.nome = nome;
        this.sobrenome = sobrenome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersao() {
        return versao;
    }

    public void setVersao(int versao) {
        this.versao = versao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    @Override
    public String toString() {
        return "Autor{" + "id=" + id + ", versao=" + versao + ", nome=" + nome + ", sobrenome=" + sobrenome + '}';
    }
}
