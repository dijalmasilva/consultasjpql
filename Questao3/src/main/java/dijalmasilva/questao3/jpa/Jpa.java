/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dijalmasilva.questao3.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * 
 * @author Dijalma Silva <dijalmacz@gmail.com>
 * @param <T>
 */
public class Jpa<T> {

    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory("Questao3");
    private final EntityManager em = factory.createEntityManager();
    
    public void salvar(T obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
    }
    
    
    //Terceira questão -> SQL nativa
    public List<T> buscarMaioresDeIdade(Class<T> classe){
        Query query = em.createNativeQuery("Select * from Pessoa where idade > 17", classe);
        return query.getResultList();
    }
    
    //Terceira questão -> SQLMapping
    public List<T> buscarAutores(){
        return em.createNativeQuery("Select a.id idAutor, a.nome, a.sobrenome, a.versao from Autor a", "MapAutor").getResultList();
    }
}
